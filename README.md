# Kfree.Gitlab.Io

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## How? 

To generate this project, I ran 

```
$ ng new kfree.gitlab.io --style=sass --verbose=true --routing=true 
  create kfree.gitlab.io/README.md (1031 bytes)
  create kfree.gitlab.io/.angular-cli.json (1252 bytes)
  create kfree.gitlab.io/.editorconfig (245 bytes)
  create kfree.gitlab.io/.gitignore (544 bytes)
  create kfree.gitlab.io/src/assets/.gitkeep (0 bytes)
  create kfree.gitlab.io/src/environments/environment.prod.ts (51 bytes)
  create kfree.gitlab.io/src/environments/environment.ts (387 bytes)
  create kfree.gitlab.io/src/favicon.ico (5430 bytes)
  create kfree.gitlab.io/src/index.html (302 bytes)
  create kfree.gitlab.io/src/main.ts (370 bytes)
  create kfree.gitlab.io/src/polyfills.ts (3114 bytes)
  create kfree.gitlab.io/src/styles.sass (80 bytes)
  create kfree.gitlab.io/src/test.ts (642 bytes)
  create kfree.gitlab.io/src/tsconfig.app.json (211 bytes)
  create kfree.gitlab.io/src/tsconfig.spec.json (283 bytes)
  create kfree.gitlab.io/src/typings.d.ts (104 bytes)
  create kfree.gitlab.io/e2e/app.e2e-spec.ts (297 bytes)
  create kfree.gitlab.io/e2e/app.po.ts (208 bytes)
  create kfree.gitlab.io/e2e/tsconfig.e2e.json (235 bytes)
  create kfree.gitlab.io/karma.conf.js (923 bytes)
  create kfree.gitlab.io/package.json (1300 bytes)
  create kfree.gitlab.io/protractor.conf.js (722 bytes)
  create kfree.gitlab.io/tsconfig.json (363 bytes)
  create kfree.gitlab.io/tslint.json (3012 bytes)
  create kfree.gitlab.io/src/app/app-routing.module.ts (245 bytes)
  create kfree.gitlab.io/src/app/app.module.ts (395 bytes)
  create kfree.gitlab.io/src/app/app.component.sass (0 bytes)
  create kfree.gitlab.io/src/app/app.component.html (1173 bytes)
  create kfree.gitlab.io/src/app/app.component.spec.ts (1103 bytes)
  create kfree.gitlab.io/src/app/app.component.ts (208 bytes)
npm WARN deprecated nodemailer@2.7.2: All versions below 4.0.1 of Nodemailer are deprecated. See https://nodemailer.com/status/
npm WARN deprecated mailcomposer@4.0.1: This project is unmaintained
npm WARN deprecated socks@1.1.9: If using 2.x branch, please upgrade to at least 2.1.6 to avoid a serious bug with socket data flow and an import issue introduced in 2.1.0
npm WARN deprecated node-uuid@1.4.8: Use uuid module instead
npm WARN deprecated buildmail@4.0.1: This project is unmaintained
npm WARN deprecated socks@1.1.10: If using 2.x branch, please upgrade to at least 2.1.6 to avoid a serious bug with socket data flow and an import issue introduced in 2.1.0

> uws@9.14.0 install /home/kus/src/kfree.gitlab.io/node_modules/uws
> node-gyp rebuild > build_log.txt 2>&1 || exit 0


> node-sass@4.8.3 install /home/kus/src/kfree.gitlab.io/node_modules/node-sass
> node scripts/install.js

Cached binary found at /home/kus/.npm/node-sass/4.8.3/linux-x64-59_binding.node

> uglifyjs-webpack-plugin@0.4.6 postinstall /home/kus/src/kfree.gitlab.io/node_modules/webpack/node_modules/uglifyjs-webpack-plugin
> node lib/post_install.js


> node-sass@4.8.3 postinstall /home/kus/src/kfree.gitlab.io/node_modules/node-sass
> node scripts/build.js

Binary found at /home/kus/src/kfree.gitlab.io/node_modules/node-sass/vendor/linux-x64-59/binding.node
Testing binary
Binary is fine
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@1.1.3 (node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@1.1.3: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})

added 1264 packages in 38.799s
You can `ng set --global packageManager=yarn`.
Project 'kfree.gitlab.io' successfully created.

```


so many errors so little time 

```
$ vim .;git add .;git commit -S;git push -u origin --all;yarn run generate;
[master 30fd40a] sigh
 1 file changed, 0 insertions(+), 0 deletions(-)
 rename src/app/{app.component.scss => app.component.sass} (100%)
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (4/4), done.
Writing objects: 100% (4/4), 995 bytes | 995.00 KiB/s, done.
Total 4 (delta 3), reused 0 (delta 0)
To gitlab.com:kfree/kfree.gitlab.io.git
   1a1b128..30fd40a  master -> master
Branch master set up to track remote branch master from origin.
yarn run v1.5.1
$ ng build --aot=true --target=production --environment=prod --build-optimizer --verbose --output-path=public
904ms building modules                                                             
0ms sealing 
0ms optimizing 
0ms basic module optimization 
0ms module optimization 
1ms advanced module optimization 
1ms basic chunk optimization        
1ms chunk optimization 
0ms advanced chunk optimization 
6ms building modules                                                               
0ms module and chunk tree optimization 
0ms chunk modules optimization 
0ms advanced chunk modules optimization 
0ms module reviving 
1ms module order optimization 
0ms module id optimization 
0ms chunk reviving 
0ms chunk order optimization 
0ms chunk id optimization 
1ms hashing 
0ms module assets processing 
1ms chunk assets processing 
2ms additional chunk assets processing 
0ms additional asset processing 
6ms chunk asset optimization 
0ms asset optimization 
Hash: 082d8830e3d67ba69216
Version: webpack 3.11.0
Time: 938ms
                                   Asset       Size  Chunks  Chunk Names
polyfills.997d8cc03812de50ae67.bundle.js   84 bytes       1  polyfills
     main.ee32620ecd1edff94184.bundle.js   84 bytes       2  main
   inline.318b50c57b4eba3d437b.bundle.js  796 bytes       3  inline
chunk    {0}  (styles) 982 bytes {3} [initial] [rendered]
chunk    {1} polyfills.997d8cc03812de50ae67.bundle.js (polyfills) 28 bytes {3} [initial] [rendered]
chunk    {2} main.ee32620ecd1edff94184.bundle.js (main) 28 bytes {3} [initial] [rendered]
chunk    {3} inline.318b50c57b4eba3d437b.bundle.js (inline) 0 bytes [entry] [rendered]

ERROR in Module build failed: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/app/app.component.sass (line 4, column 5)

ERROR in ./src/styles.sass
Module build failed: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
Error: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
    at options.error (/home/kus/src/kfree.gitlab.io/node_modules/node-sass/lib/index.js:291:26)
 @ multi ./src/styles.sass

ERROR in ./src/styles.sass
Module build failed: ModuleBuildError: Module build failed: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
    at runLoaders (/home/kus/src/kfree.gitlab.io/node_modules/webpack/lib/NormalModule.js:195:19)
    at /home/kus/src/kfree.gitlab.io/node_modules/loader-runner/lib/LoaderRunner.js:364:11
    at /home/kus/src/kfree.gitlab.io/node_modules/loader-runner/lib/LoaderRunner.js:230:18
    at context.callback (/home/kus/src/kfree.gitlab.io/node_modules/loader-runner/lib/LoaderRunner.js:111:13)
    at Object.asyncSassJobQueue.push [as callback] (/home/kus/src/kfree.gitlab.io/node_modules/sass-loader/lib/loader.js:55:13)
    at Object.done [as callback] (/home/kus/src/kfree.gitlab.io/node_modules/neo-async/async.js:7921:18)
    at options.error (/home/kus/src/kfree.gitlab.io/node_modules/node-sass/lib/index.js:294:32)
 @ ./src/styles.sass
 @ multi ./src/styles.sass
Child html-webpack-plugin for "index.html":
         Asset     Size  Chunks  Chunk Names
    index.html  2.87 kB       0  
    chunk    {0} index.html 351 bytes [entry] [rendered]
Child extract-text-webpack-plugin node_modules/extract-text-webpack-plugin/dist node_modules/raw-loader/index.js!node_modules/postcss-loader/lib/index.js??extracted!node_modules/sass-loader/lib/loader.js??ref--9-3!src/styles.sass:
    chunk    {0} extract-text-webpack-plugin-output-filename 189 bytes [entry] [rendered]
    
    ERROR in ./node_modules/raw-loader!./node_modules/postcss-loader/lib??extracted!./node_modules/sass-loader/lib/loader.js??ref--9-3!./src/styles.sass
    Module build failed: 
    h1 {
       ^
          Invalid CSS after "h1 {": expected "}", was "{"
          in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
    Error: 
    h1 {
       ^
          Invalid CSS after "h1 {": expected "}", was "{"
          in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
        at options.error (/home/kus/src/kfree.gitlab.io/node_modules/node-sass/lib/index.js:291:26)

ERROR in Module build failed: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/app/app.component.sass (line 4, column 5)
ERROR in ./src/styles.sass
Module build failed: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
Error: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
    at options.error (/home/kus/src/kfree.gitlab.io/node_modules/node-sass/lib/index.js:291:26)
 @ multi ./src/styles.sass
ERROR in ./src/styles.sass
Module build failed: ModuleBuildError: Module build failed: 
h1 {
   ^
      Invalid CSS after "h1 {": expected "}", was "{"
      in /home/kus/src/kfree.gitlab.io/src/styles.sass (line 6, column 5)
    at runLoaders (/home/kus/src/kfree.gitlab.io/node_modules/webpack/lib/NormalModule.js:195:19)
    at /home/kus/src/kfree.gitlab.io/node_modules/loader-runner/lib/LoaderRunner.js:364:11
    at /home/kus/src/kfree.gitlab.io/node_modules/loader-runner/lib/LoaderRunner.js:230:18
    at context.callback (/home/kus/src/kfree.gitlab.io/node_modules/loader-runner/lib/LoaderRunner.js:111:13)
    at Object.asyncSassJobQueue.push [as callback] (/home/kus/src/kfree.gitlab.io/node_modules/sass-loader/lib/loader.js:55:13)
    at Object.done [as callback] (/home/kus/src/kfree.gitlab.io/node_modules/neo-async/async.js:7921:18)
    at options.error (/home/kus/src/kfree.gitlab.io/node_modules/node-sass/lib/index.js:294:32)
 @ ./src/styles.sass
 @ multi ./src/styles.sass
error An unexpected error occurred: "Command failed.
Exit code: 1
Command: sh
Arguments: -c ng build --aot=true --target=production --environment=prod --build-optimizer --verbose --output-path=public
Directory: /home/kus/src/kfree.gitlab.io
Output:
".
info If you think this is a bug, please open a bug report with the information provided in "/home/kus/src/kfree.gitlab.io/yarn-error.log".
info Visit https://yarnpkg.com/en/docs/cli/run for documentation about this command.

```
